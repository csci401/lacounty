﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CSCI 401</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php
include "php/session.php";

	session_start();
	checkForActiveSession();
	
	//$run = include 'config.php';
	//if (!$run) {
	//	die('process abort');
	//} else {
	//	require_once("./mailfunctions.php");
	//	$time = 15; // second, if one day, 60*60*24
	//	$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	//	// send mail
	//	// select table
	//	// MySQL config
	//	$DB_HOST = "localhost";
	//	$DB_USER = "root";
	//	$DB_PASSWORD = "root";
	//	$DB_NAME = "prometheus";
	//	// query all course
	//	$conn = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);
	//	$query = "select * from asset";
	//	$stmt = mysqli_prepare($conn, $query);
	//	mysqli_stmt_execute($stmt);
	//	$results = mysqli_stmt_get_result($stmt);
	//	$rowcount = mysqli_num_rows($results);
	//	if ($rowcount > 0) {
	//		sendMail('369397202@qq.com','PHP Server','Table not null------'.$rowcount.'rows');
	//	}
	//	sleep($time);
	//	file_get_contents($url);
	//}
?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Tony Sereno</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Last access : 30 Jan 2017 &nbsp; <a href="php/process_logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a class="active-menu"  href="index.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                    <li  >
                        <a  href="form.php"><i class="fa fa-edit fa-3x"></i> Asset </a>
                    </li>   

                     <li>
                        <a href="search.php"><i class="fa fa-sitemap fa-3x"></i> Search</a>
                      </li> 
                       <li  >
                                 <a  href="table.php"><i class="fa fa-table fa-3x"></i> Table Examples</a>
                        </li>

                           <li >
                             <a  href="mobilescan.html"><i class="fa fa-table fa-3x"></i> mobile scan</a>
                          </li>
                          <li >
                             <a  href="pendding.php"><i class="fa fa-table fa-3x"></i> All Pending Request</a>
                          </li>

                      <li><a></a></li>
                    <li><a></a></li>

                      <li><a></a></li>
                      <li><a></a></li>
                       <li><a></a></li>
                    <li><a></a></li>

                      <li><a></a></li>
                      <li><a></a></li>


                     <li>
                        <a  href="ui.php"><i class="fa fa-desktop fa-3x"></i> UI Elements</a>
                    </li>
                    <li>
                        <a  href="tab-panel.php"><i class="fa fa-qrcode fa-3x"></i> Tabs & Panels</a>
                    </li>
						   <li  >
                        <a   href="chart.php"><i class="fa fa-bar-chart-o fa-3x"></i> Morris Charts</a>
                    </li>	
                      <li  >
                        <a  href="table.php"><i class="fa fa-table fa-3x"></i> Table Examples</a>
                    </li>
                    			
					
					                   
                    
                  <li  >
                        <a  href="blank.php"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li>	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Admin Dashboard</h2>   
                        <h5>Welcome Tony Sereno, Love to see you back. </h5>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-warning"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">2 Alerts</p>
                    <p class="text-muted">Changes</p>
                </div>
             </div>
             </div>
                <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-desktop"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"> 9 Device</p>
                    <p class="text-muted">Under My Name</p>
                </div>
             </div>
		     </div>
             <div class="col-md-3 col-sm-6 col-xs-6">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-bell-o"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">10 New</p>
                    <p class="text-muted">Requests</p>
                </div>
             </div>
             </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-bars"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">4 Recent</p>
                    <p class="text-muted">Transaction</p>
                </div>
             </div>
		     </div>
                    

			</div>
                 <!-- /. ROW  -->
                <hr />                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">           
			<div class="panel panel-back noti-box">
                <span class="icon-box ">
                    <i class="fa fa-warning"></i>
                </span>
                <div class="text-box" >
                                     <button type="button" class="btn btn-default btn-circle" style="float: right;"><i class="fa fa-check"></i></button>

                    <p class="main-text">2 Alerts: Changes in equipment ownership occur</p>
                    <hr style="clear: both;" />
                    <p class="text-muted">
                          <span class="text-muted color-bottom-txt"><i class="fa fa-edit"></i>
                               Asset #52133 removeed from your decive list with comment "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. "
                               </span>
                    </p>
                    <p class="text-muted">Time: 13 mins ago</p>
                    <hr />
                    <p class="text-muted">
                          <span class="text-muted color-bottom-txt"><i class="fa fa-edit"></i>
                               Asset #52133 added to your decive list with comment "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.". 
                               </span>

                    </p>
                    <p class="text-muted">Time: 14 mins ago</p>
                 


                </div>
             </div>
		     </div>
                    
                    
                    
                        
        </div>
                 <!-- /. ROW  -->
            
                <div class="row" >
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
               
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <i class="fa fa-desktop"></i>
                           Devices Under My Name
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Asset ID</th>
                                            <th>Location</th>
                                            <th>Serialnbr</th>
                                            <th>Categorydesc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>100090</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>100090</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                            <td>100090</td>
                                        </tr>
                                         <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>100090</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>100090</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                            <td>100090</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
                <!-- end of row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <i class="fa fa-bell fa-fw"></i>
                    New Requests               
                     </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
<!-- //////////////////////////////////////// -->
<?php
    $servername = "localhost:3306";
    $username = "root";
    $password = "root";
    $dbname = "prometheus";

    $connection = new mysqli($servername, $username, $password, $dbname);

    $sql = "SELECT * FROM PenddingRequest";
    $result = $connection->query($sql);
    $count=0;
      if ($result->num_rows > 0) {
          while($row = $result->fetch_array(MYSQLI_NUM)) {
          echo ' <div class="panel panel-default" id = "', $row[1],'">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#';
                                            echo $count;
                                            echo'" class="collapsed">';
                                            echo "Asset Tag #", $row[1];
                                            echo '</a>
                                        </h2>
                                    </div>
                                    <div id="';
                                    echo $count;
                                    echo'" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">';
                                        echo "Requset: Asset ", $row[1], " change location to ", $row[22];
                                        if (checkRole()){

                                        echo '<br>
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" style="float: right; margin-left: 10px;" onclick="disapprove(\'',$row[1], '\')">
                                            <i class="glyphicon glyphicon-remove "></i></button>   
                                            <button type="button" class="btn btn-info btn-circle btn-lg" style="float: right; display: true;" onclick="approve(\'',$row[1], '\')">

                                            <i class="glyphicon glyphicon-ok"></i></button>
                                            <br style="clear: both;">';}
                                        echo '

                                        </div>
                                    </div>
                                </div>';
                                        $count++;


        }

    }
?> 



                                

<!-- //////////////////////////////////////// -->


                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>


                 <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                   
                    <div class="chat-panel panel panel-default chat-boder chat-panel-head" >
                        <div class="panel-heading">
                            <i class="fa fa-bars fa-fw"></i>
                                Recent Transaction                            
                                <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                                <ul class="dropdown-menu slidedown">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-refresh fa-fw"></i>Refresh
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-check-circle fa-fw"></i>Available
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-times fa-fw"></i>Busy
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-clock-o fa-fw"></i>Away
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-sign-out fa-fw"></i>Sign Out
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">
                            <ul class="chat-box">
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="assets/img/1.png" alt="User" class="img-circle" />
                                    </span>
                                    <div class="chat-body">                                        
                                            <strong >Asset #23232</strong>
                                            <small class="pull-right text-muted">
                                                <i class="fa fa-clock-o fa-fw"></i>12 mins ago
                                            </small>                                      
                                        <p>
                                            Asset #23232 Added to A.Sanchez.
                                        </p>
                                    </div>
                                </li>

                                <li class="right clearfix">
                                    <span class="chat-img pull-right">

                                        <img src="assets/img/2.png" alt="User" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                          <strong class="pull-right">Asset #52133</strong>

                                            <small class=" text-muted">
                                                <i class="fa fa-clock-o fa-fw"></i>13 mins ago</small>
                                      
                                        <p>
                                            Asset #52133 removeed from Vahe.<br>
                                            Comments: ...
                                        </p>
                                    </div>
                                </li>

                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                         <img src="assets/img/3.png" alt="User" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        
                                            <strong >Asset #AS20145</strong>
                                            <small class="pull-right text-muted">
                                                <i class="fa fa-clock-o fa-fw"></i>14 mins ago</small>
                                        
                                        <p>
                                            Asset #AS20145 added to workstation.
                                        </p>
                                    </div>
                                </li>
                                <li class="right clearfix">
                                    <span class="chat-img pull-right">
                                         <img src="assets/img/4.png" alt="User" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                      
                                            <small class=" text-muted">
                                                <i class="fa fa-clock-o fa-fw"></i>15 mins ago</small>
                                            <strong class="pull-right">Asset #AS20145 </strong>
                                       
                                        <p>
                                            Asset #AS20145 added to workstation.
                                            <br>
                                            Comments: ...
                                        </p>
                                    </div>
                                </li>
                                    <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="assets/img/1.png" alt="User" class="img-circle" />
                                    </span>
                                    <div class="chat-body">                                        
                                            <strong >Angel Sanchez</strong>
                                            <small class="pull-right text-muted">
                                                <i class="fa fa-clock-o fa-fw"></i>12 mins ago
                                            </small>                                      
                                        <p>
                                        All devices under Angel Sanchez added to Tony.
                                        <br>
                                        Details:<br>
                                        <ul>
                                            <li>
                                             Asset #23232 Added to Tony.</li>                                            
                                             <li>

                                            Asset #23233 Added to Tony.
                                            </li>
                                            <li>
                                            Asset #23234 Added to Tony.
                                            </li>
                                            <li>
                                            Asset #23235 Added to Tony.

                                            </li>
                                        </ul>

                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    
                </div>
                   
                </div>     
                 <!-- /. ROW  -->

    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

    <script>
        function approve(object){
            // console.log("hello");
            console.log(object);
            // return;


            $.ajax({
                type: "POST",
                url: 'php/approvePending.php',
                data:{ 
                    "tag": object,
                },
                success:function(html) {
                    alert(html);
                 }

            });
            

            document.querySelector("#"+object).remove();
            

        }

        function disapprove(object){
            // alert("dis");
             document.querySelector("#"+object).remove();

        }
    </script>
    
   
</body>
</html>
