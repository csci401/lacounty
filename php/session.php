<?php
	function checkForActiveSession() {
		if (!userLoggedIn()) {
			header("Location: http://localhost/lacounty/login.html");
		}
	}
	
	function userLoggedIn() {
		return $_SESSION["logged_in"];
	}

	function checkRole(){
		return $_SESSION["role"];
	}
?>